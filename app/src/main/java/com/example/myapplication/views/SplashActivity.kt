package com.example.myapplication.views

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.example.myapplication.R
import com.example.myapplication.util.MotivationConstants
import com.example.myapplication.util.SecurityPreferences
import kotlinx.android.synthetic.main.activity_splash.*

class SplashActivity : AppCompatActivity() {

    private lateinit var mSecurity: SecurityPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        mSecurity = SecurityPreferences(applicationContext)

        buttonSave.setOnClickListener { v: View ->
            handleSave()
        }
    }

    private fun isValid(): Boolean {
        return editTextName.text.toString() != ""
    }

    private fun handleSave() {
        if (!isValid()) {
            Toast.makeText(applicationContext, getString(R.string.name_required), Toast.LENGTH_LONG).show()
            return
        }
        val username = editTextName.text.toString()
        mSecurity.storeString(MotivationConstants.KEY.USERNAME, username)

        val intent: Intent = Intent(applicationContext, Main2Activity::class.java)
        startActivity(intent)

        finish()
    }
}
