package com.example.myapplication.views

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.myapplication.R
import com.example.myapplication.util.MotivationConstants
import com.example.myapplication.util.SecurityPreferences
import kotlinx.android.synthetic.main.activity_main2.*

class Main2Activity : AppCompatActivity() {

    private lateinit var mSecurity: SecurityPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)

        userName()
    }

    fun userName() {
        mSecurity = SecurityPreferences(applicationContext)

        val username = mSecurity.getStoredString(MotivationConstants.KEY.USERNAME)

        textUsername.text = username;
    }
}
