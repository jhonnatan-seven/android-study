package com.example.myapplication.views

import androidx.appcompat.app.AppCompatActivity

import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.example.myapplication.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        buttonCalc.setOnClickListener { v: View? ->
            handleCalculate()
        }

//        buttonCalc.setOnClickListener(this)
    }

    override fun onClick(view: View) {
//        if (view.id == R.id.buttonCalc) {
//            clickButtonCalc()
//        }
    }

    private fun handleCalculate() {
        if (!isValid()) {
            Toast.makeText(applicationContext, "Todos os campos são obrigatórios!", Toast.LENGTH_LONG).show()
            return
        }
        val value = ((editTextDistance.text.toString().toFloat() * editTextPrice.text.toString().toFloat()) / editTextAutonomy.text.toString().toFloat())
                .toDouble()
                .toString()
                .replace(".", ",")

        textViewValue.text = "Total: R$ $value"
    }

    private fun isValid(): Boolean {
        if (editTextDistance.text.toString() == ""
                || editTextPrice.text.toString() == ""
                || editTextAutonomy.text.toString() == ""
                || editTextAutonomy.text.toString() == "0"
        ) {
            return false
        }
        return true
    }
}
